.PHONY: test test_official_pkgs test_langs_pkgs test_pkgs test_gui_pkgs test_rust_crates test_aur_pkgs setup_nodejs setup_python test_python_pip test_python_pipx docker_build

test: test_official_pkgs test_aur_pkgs test_rust_crates

test_official_pkgs: test_pkgs test_gui_pkgs test_langs_pkgs

test_pkgs:
	./bin/check_all_names ./pkgs

test_gui_pkgs:
	./bin/check_all_names ./gui_pkgs

test_langs_pkgs:
	./bin/check_all_names ./langs_pkgs

test_aur_pkgs:
	which curl > /dev/null || sudo pacman --sync --needed --noconfirm curl
	./bin/check_all_names_aur ./aur

test_rust_crates:
	sudo pacman --sync --needed --noconfirm curl
	./bin/check_all_rust_crates

setup_nodejs:
	sudo pacman --sync --needed --noconfirm nodejs npm
	npm install --global $$(cat ./lang_managed/node.npm.txt)

setup_python:
	sudo pacman --sync --needed --noconfirm python python-pipx

python_venv: setup_python
	python -m venv python_venv

test_python_pip: python_venv
	source python_venv/bin/activate && \
		pip install --requirement ./lang_managed/python.pip.txt

test_python_pipx: python_venv
	source python_venv/bin/activate && \
		cat ./lang_managed/python.pipx.txt | xargs --max-args 1 pipx install

docker_build: Dockerfile pkgs/ langs_pkgs/ lang_managed/ aur/
	docker build -t auto_arch .

