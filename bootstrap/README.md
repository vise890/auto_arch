# Bootstrap scripts

Use these to create a bare bones Arch Linux system.

# Usage

Boot into a live Arch Linux instance (from CD/USB), and then:

```bash
# get auto_arch
wget https://gitlab.com/vise890/auto_arch/repository/master/archive.tar.gz
tar xzf archive.tar.gz
cd auto_arch*/bootstrap/

# check / modify variables
vim 0_variables.sh

# prepare the disk
./1_prepare_disk.sh

# install the base system
# NOTE once this is done, the system will reboot
./2_bootstrap.sh

```

Login with `root:$DEFAULT_PASSWORD` (set in `./0_variables.sh`)
