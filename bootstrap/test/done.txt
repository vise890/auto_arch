x fix all the file copyign in the ansible playbook
x (A) fix: vagrant user cannot ssh into box:
x (A) fix sudoers
x (A) fix vagrant network interfaces
x (A) create $DEFAULT_USER, copy current user's rsa_id.pub into their ~/.ssh/authorized_hosts
x (A) move paker/*.sh into folders (install, provision)
x (A) fix locale
x (A) pacstrap base-devel
x (B) only install --needed packages
x (B) add [haskell-core] to pacman.conf +haskell
x (B) install haskell-platform packages +haskell
x (A) update pacman trustdb
x setup sync with hardware clock
x (A) remove happstack
x (A) import haskell PGP key 048D/4209170B
x add key and check for signature of [haskell-core] packages
x (A) clone and install dotfiles repo
x (B) split install and provisioning scripts (use with real machine as well as VM)
x (B) setup +aur (build packer/aura)
x (A) download auto_arch instead of having it as a submodule
x (A) spike: can i mount a folder external to the VM onto /var/pacman/cache? (template.json)
