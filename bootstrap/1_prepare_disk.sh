#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

source './0_variables.sh'

echo '==> preparing the disk'
sgdisk --zap-all "$DISK"
sgdisk --clear "$DISK"

sgdisk --new=1:0:+1GB "$DISK" --typecode=1:ef00 --change-name=1:"ESP" # EFI System Partition -> /boot
sgdisk --new=2:0:0 "$DISK"    --typecode=2:8300 --change-name=2:"/"   # linux fs root -> /

# https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#Preparing_non-boot_partitions

mkfs.fat -F32 "${DISK}1"
mkfs.ext4 "${DISK}2"

echo "==> mounting partitions.. on $ROOT_MOUNTPOINT"

mount "${DISK}2" "$ROOT_MOUNTPOINT"
mkdir -p "${ROOT_MOUNTPOINT}/boot"

mount "${DISK}1" "${ROOT_MOUNTPOINT}/boot"


