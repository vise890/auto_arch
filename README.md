# Auto Arch

Auto Arch is:
 - Install helpers (in `./bootstrap/`)
 - Other helpers
 - Package lists

```
.
├── aur           # Package found in the AUR
├── bootstrap     # Arch linux install helpers
├── gui_pkgs      # Graphical environment packages
├── lang_managed  # Language-specific packages (gems, eggs, crates, etc.)
├── pkgs          # Mostly CLI packages that I definitely want everywhere
├── sh            # Some helper scripts
└── systemd.sh    # Scripts to enable systemd units
```

## Usage

```bash
sudo pacman -S $(cat gui_pkgs/sway.pkgs ) --needed
```
