#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

sudo --validate

echo "==> installing base-devel package group and needed pkgs"
sudo pacman -S --needed --noconfirm base-devel wget

echo "==> creating yay user"
id --user yay &>/dev/null || sudo useradd --create-home --home-dir /tmp/yay yay

(cd /tmp

  echo "==> Naughtily cleaning previous install #yolo"
  sudo rm -rf yay.tar.gz ./yay/

  echo "==> fetching yay snapshot"
  wget "https://aur.archlinux.org/cgit/aur.git/snapshot/yay.tar.gz"

  echo "==> extracting yay"
  tar -xvf yay.tar.gz && rm yay.tar.gz

  sudo chown -R yay yay
  (cd yay
    echo "==> making yay package (makepkg)"
    # FIXME: there must be a better way to install deps as root
    sudo pacman -S --needed --noconfirm sudo git go
    # FIXME: ... and build as non sudoer
    sudo -u yay makepkg -s

    echo "==> installing yay"
    ls -la
    sudo pacman -U --noconfirm yay*.tar.xz

    echo "==> have a look at /etc/makepkg.conf for more configuration options"
  )
)



