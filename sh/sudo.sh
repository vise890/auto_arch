#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo "==> installing sudo"
pacman -S --noconfirm --needed sudo

echo "==> allow all members of wheel to be sudoers"
sed --in-place 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers
visudo --check # abort if /etc/sudoers became corrupted
