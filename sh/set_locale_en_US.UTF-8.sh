#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

sudo --validate

LOCALE="en_US.UTF-8"

echo "==> setting locale to $LOCALE"
LOCALE_GEN="${LOCALE} UTF-8"
sudo sed --in-place "s/#${LOCALE_GEN}/${LOCALE_GEN}/" /etc/locale.gen
sudo locale-gen
echo "LANG=\"$LOCALE\"" | sudo tee /etc/locale.conf
