#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

sudo --validate

echo "==> optimizing pacman mirrorlist"
sudo pacman -S --noconfirm --needed reflector
sudo reflector -l 50 -p http --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Syy --noconfirm
