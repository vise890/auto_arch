FROM archlinux:base-devel
# FROM registry.gitlab.com/vise890/auto_arch:latest

MAINTAINER Martino Visintin

ADD . ~/auto_arch

WORKDIR ~/auto_arch

RUN ./sh/upgrade_system.sh


ENTRYPOINT ["make"]
