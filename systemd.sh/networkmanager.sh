#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

echo "==> Installing networkmanager packages"
sudo pacman --sync --noconfirm --needed \
    $(cat ./pkgs/networkmanager.pkgs)

echo "==> Enabling NetworkManager service"
systemctl enable NetworkManager

